# AdventOfCode22.jl
[![pipeline](https://gitlab.mpcdf.mpg.de/fresch/AdventOfCode22.jl/badges/master/pipeline.svg?job=pipeline)](https://gitlab.mpcdf.mpg.de/fresch/AdventOfCode22.jl/-/pipelines)[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://fresch.pages.mpcdf.de/AdventOfCode22.jl/)

| Day | Time | Number of allocations | Allocated memory |
|----:|-----:|----------------------:|-----------------:|
| 1 | 232.058 μs | 3103| 376.56 KiB |
| 2 | 165.957 μs | 16| 162.03 KiB |
| 3 | 713.543 μs | 9036| 794.06 KiB |
| 4 | 350.861 μs | 3007| 424.33 KiB |
| 5 | 350.199 μs | 5171| 323.61 KiB |
| 6 | 40.973 μs | 8| 1.56 KiB |
| 7 | 232.278 μs | 4491| 467.89 KiB |
| 8 | 406.232 μs | 24| 173.97 KiB |
| 9 | 2.083 ms | 61| 617.28 KiB |
| 10 | 15.743 μs | 603| 28.82 KiB |
| 11 | 314.740 ms | 7715821| 234.30 MiB |
| 12 | 70.969 ms | 9697| 131.96 MiB |
| 13 | 1.338 ms | 27682| 1.74 MiB |
| 14 | 307.161 ms | 15912| 3.04 MiB |
| 15 | 569.078 ms | 1384| 421.63 MiB |
| 16 | 478.830 ms | 10293138| 557.34 MiB |
| 17 | 155.552 ms | 667610| 161.72 MiB |
| 18 | 4.392 ms | 10679| 3.28 MiB |
| 19 | 19.781 s | 2108| 13.55 GiB |
| 20 | 451.437 ms | 14805| 1.15 MiB |
| 21 | 2.018 ms | 16231| 1.79 MiB |
| 22 | 14.774 ms | 9150| 962.13 KiB |
| 23 | 822.317 ms | 28538| 141.01 MiB |
| 24 | 397.711 ms | 68420| 448.55 MiB |
| 25 | 62.618 μs | 810| 61.23 KiB |