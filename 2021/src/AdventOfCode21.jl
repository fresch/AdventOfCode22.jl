module AdventOfCode21

    using BenchmarkTools
    using Printf

    # Functionality inspired by https://github.com/goggle/AdventOfCode2022.jl

    solvedDays = [1, 2, 3, 18]

    # Include the source files:
    for day in solvedDays
        ds = @sprintf("%02d", day)
        include(joinpath(@__DIR__, "day$ds.jl"))
    end

    function readInput(path::String)
        s = open(path, "r") do file
            read(file, String)
        end
        return s
    end
    
    function readInput(day::Int)
        path = joinpath(@__DIR__, "..", "inputs", @sprintf("day%02d.txt", day))
        return readInput(path)
    end

    # Export readInput for the modules of each day: 
    export readInput

    # Export a function `dayXY` for each day:
    for d in solvedDays
        global ds = @sprintf("day%02d.txt", d)
        global modSymbol = Symbol(@sprintf("Day%02d", d))
        global dsSymbol = Symbol(@sprintf("day%02d", d))

        @eval begin
            function $dsSymbol(input::String = AdventOfCode21.readInput(ds))
                return AdventOfCode21.$modSymbol.$dsSymbol(input)
            end
            export $dsSymbol
        end
    end

end # module AdventOfCode21
