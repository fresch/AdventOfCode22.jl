using AdventOfCode22.AdventOfCode21

using Test

@testset "Day 1" begin
    @test AdventOfCode21.Day01.day01() == [1184, 1158]
end

@testset "Day 2" begin
    @test AdventOfCode21.Day02.day02() == [1727835, 1544000595]
end

@testset "Day 3" begin
    @test AdventOfCode21.Day03.day03() == [4139586, 1800151]
end


@testset "Day 18" begin
    @test AdventOfCode21.Day18.day18() == [4323, 4749]
end