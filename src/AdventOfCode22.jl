module AdventOfCode22

    using BenchmarkTools
    using Printf

    # Functionality inspired by https://github.com/goggle/AdventOfCode2022.jl

    solvedDays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]

    # Include the source files:
    for day in solvedDays
        ds = @sprintf("%02d", day)
        include(joinpath(@__DIR__, "day$ds.jl"))
    end

    # Include utility routines: 
    include("util.jl")

    # Export readInput for the modules of each day: 
    export readInput

    # Export a function `dayXY` for each day:
    for d in solvedDays
        global ds = @sprintf("day%02d.txt", d)
        global modSymbol = Symbol(@sprintf("Day%02d", d))
        global dsSymbol = Symbol(@sprintf("day%02d", d))

        @eval begin
            input_path = joinpath(@__DIR__, "..", "inputs", ds)
            function $dsSymbol(input::String = AdventOfCode22.readInput($d))
                return AdventOfCode22.$modSymbol.$dsSymbol(input)
            end
            export $dsSymbol
        end
    end


    # Include last year
    include(joinpath(@__DIR__, "../2021/src/AdventOfCode21.jl"))
end # module AdventOfCode22
