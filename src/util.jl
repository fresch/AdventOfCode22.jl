# Read the input from a file:
function readInput(path::String)
    s = open(path, "r") do file
        read(file, String)
    end
    return s
end

function readInput(day::Int)
    path = joinpath(@__DIR__, "..", "inputs", @sprintf("day%02d.txt", day))
    return readInput(path)
end

# Benchmark a list of different problems:
function benchmark(days::Vector{Int}=solvedDays)
    results = []
    for day in days
        modSymbol = Symbol(@sprintf("Day%02d", day))
        fSymbol = Symbol(@sprintf("day%02d", day))
        input = readInput(joinpath(@__DIR__, "..", "inputs", @sprintf("day%02d.txt", day)))
        @eval begin
            bresult = @benchmark(AdventOfCode22.$modSymbol.$fSymbol($input))
        end
        push!(results, (day, time(bresult), allocs(bresult), memory(bresult)))
    end
    return results
end

# Write the benchmark results into a markdown string:
function _to_markdown_table(bresults::Vector)
    header = "| Day | Time | Number of allocations | Allocated memory |\n" *
             "|----:|-----:|----------------------:|-----------------:|"

    lines = [header]
    for (d, t, a, m) in bresults
        ds = string(d)
        ts = BenchmarkTools.prettytime(t)
        ms = BenchmarkTools.prettymemory(m)
        push!(lines, "| $ds | $ts | $a| $ms |")
    end
    return join(lines, "\n")
end

# Add benchmarks to the README.md
function _write_benchmarks_to_README()
    str = _to_markdown_table(benchmark(solvedDays))
    readme = "# AdventOfCode22.jl\n[![pipeline](https://gitlab.mpcdf.mpg.de/fresch/AdventOfCode22.jl/badges/master/pipeline.svg?job=pipeline)](https://gitlab.mpcdf.mpg.de/fresch/AdventOfCode22.jl/-/pipelines)[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://fresch.pages.mpcdf.de/AdventOfCode22.jl/)\n\n"

    open("README.md", "w") do io
        write(io, join([readme, str]))
    end
end